# Raspberry Pi

- It is low cost ,credit card size computer.
- It is the name of a series of small single board computer made by Raspberry foundation. 

## Components of Rasberry Pi
![](https://raw.githubusercontent.com/SeeedDocument/Raspberry-Pi-4/master/img/hardware-1024.jpg)


### USB 
USB ports are used to connect a wide variety of components, most commonly a mouse and keyboard.

### HDMI 
HDMI port outputs video and audio to the monitor.

### Audio
Audio jack allows to connect standard headphones and speakers.

### Micro USB
Micro USB port is only for power, do not connect anything else to this port. .

### GPIO 
The GPIO ports allow the Raspberry Pi to control and take input from any electronic component.

### SD card slot 
- The Raspberry Pi uses SD cards the same way a full-size computer uses a hard drive. 
- The SD card provides the Raspberry Pi with internal memory, and stores the hard drive.

## Details of GPIO pins
![](https://bloggerbrothers.files.wordpress.com/2017/01/rp2_pinout.png?w=663)

![](https://image.slidesharecdn.com/raspberrypi01082014-140801034744-phpapp01/95/raspberry-pi-introduction-8-638.jpg?cb=1406865019)

![](https://image.slidesharecdn.com/raspberrypi01082014-140801034744-phpapp01/95/raspberry-pi-introduction-9-638.jpg?cb=1406865019)

![](https://image.slidesharecdn.com/raspberrypi01082014-140801034744-phpapp01/95/raspberry-pi-introduction-23-638.jpg?cb=1406865019)

### Raspberry Pi uses a Linux-based operating system, so  here are some of the most basic commands :

#### man – Manual
This command requests the manual page for a given command. 

#### cd – Change Directory
This command tells the Raspberry Pi where to perform the commands .

#### pwd – Print Working Directory
pwd tell about the current directory.

#### ls – List
This command returns a list of all the files and folders in the current directory.

#### nano 
nano is one of several text editors available through Terminal, and allows to edit settings and files through Terminal. 

#### tree
This command will show current directory and all the subdirectories and files it contains, displayed in an indented file structure.

#### find
This searches the Raspberry Pi for any files or folders.

#### kill 
This command is used to terminate specified process.

