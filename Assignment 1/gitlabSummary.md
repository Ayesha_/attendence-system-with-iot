# Gitlab


***Git is version-control software.***


***It keep track of every revision during the development of software. We can  share one repository of code that is worked on independently and then merged back together.There is no need to be connected all the time because the project is saved both locally on each machine and remotely (probably Gitlab).***


***GitLab group is an assemblage of projects, along with data about how users can access these projects. Each group has a project namespace.***


#### Repository

- A repository is the collection of files and folders that we are using git to track.
- Repository consists of the entire history of changes to the project.

#### Commit

- Gathering up the files exising  at that moment and putting them in a time capsule.

#### Push

- Push syncing our commits to the cloud. 

#### Branch

- It can maintain  the integrity of the software and have a way to revert if someone  do something wrong.

#### Merge

- It is integrating(combining)  two branches together.

#### Clone

- It takes the online repository and makes an exact copy of it on local machine.

#### Fork

- It make a duplicate of an existing repsitory on our local machine,by this we get an entirely new repsitory of that code under our own name.

#### Pull Request

- A pull request is when we  submit a request for the changes made (either on a branch or a fork) to merged into the Master Branch of the repository


## Git Workflow 

The basic Git work flow goes something like this:
1. clone the repsoitory 
```sh
$ git clone <link-to-repository> 
```
2. Create a new branch 
```sh
$ git checkout master
$ git checkout -b <your-branch-name>
```
3. Make changes and commit inside the repository
```
$ git status        //Check the files to which you made changes .
$ git add .         //To add untracked files( . adds all files)
$ git commit -sav   //Description about the commit 
$ git branch -a 	// Confirm working on your own branch & not master

```
4. Push changes to branch
```
$ git push origin <branch-name>    	// push changes into repository into your own branch  

```
