> ## I AM NEVER GOING TO HAVE FREE TIME EVER AGAIN

![](https://image.slidesharecdn.com/pankajkumar30262tedtalkpersonalbranding-160221175927/95/mbaskillsin-how-to-learn-anything-7-638.jpg?cb=1457349421)

It takes only **20 hrs** of focused practice to aquire any skill
instead of **10,000 hrs** for a expert level performance. 

## steps requires for focused learnig


![](https://image.slidesharecdn.com/pankajkumar30262tedtalkpersonalbranding-160221175927/95/mbaskillsin-how-to-learn-anything-14-638.jpg?cb=1457349421) 

**The first thing is to decide what we  want to learn, our task and then break it down into smaller pieces which allows us to focus on desired outcome .**


![](https://image.slidesharecdn.com/pankajkumar30262tedtalkpersonalbranding-160221175927/95/mbaskillsin-how-to-learn-anything-16-638.jpg?cb=1457349421)

**we need enough resources to to self-correct i.e we need the ability to recognize our own mistake and then make adjustment .** 


![](https://image.slidesharecdn.com/pankajkumar30262tedtalkpersonalbranding-160221175927/95/mbaskillsin-how-to-learn-anything-18-638.jpg?cb=1457349421) 

**Stay away from social media,entrtainmaints,  etc to improve  concentration during the learning phase.**

**Focus on improving  environment better and make it suitable for preactice .**


![](https://image.slidesharecdn.com/pankajkumar30262tedtalkpersonalbranding-160221175927/95/mbaskillsin-how-to-learn-anything-19-638.jpg?cb=1457349421)

**Practice atleast 20 hours to learn it in a day otherwise 45 minutes of practice daily for month.**

**The practice must be focused practice.**


![](https://2.bp.blogspot.com/-e-QIWlMibK0/W4qbjJPzyEI/AAAAAAAAGGo/_Ooh3qZ6dxALdAA5bR_V_q2-EFaBH2gPgCLcBGAs/s1600/2018-09-01.png) 


**This graph depict performace time versus practice time, explains at startin
it always take more time of practice, but it gradually decrease as we kee
practising and keep learning about the skill.**


> ## The major barrier to skill acquizition isn't intellectual, its emotional.

Nobody wants to feel stupid .Nobody wants to feel scared .It does not feel good but that is what happens  when we begin learning new skills.

**Learning dont require free time it require only dedication,confidence, and desire
to learn new skills.**



